<?php

use Illuminate\Database\Seeder;

class TabelJenisBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $faker = \Faker\Factory::create();

    	DB::table('jenis_barang')->truncate();

    	DB::table('jenis_barang')->insert([
            'jenis_barang' => 'pecah belah',
            'created_at' => now(),
            'updated_at' => now()
       	]);

    	DB::table('jenis_barang')->insert([
            'jenis_barang' => 'elektronik',
            'created_at' => now(),
            'updated_at' => now()
       	]);

    }
}
