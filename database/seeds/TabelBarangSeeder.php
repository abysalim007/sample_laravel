<?php

use Illuminate\Database\Seeder;

class TabelBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('barang')->truncate();

    	DB::table('barang')->insert([
            'kode_barang' => 'B001',
			'nama_barang' => 'Piring kenangan',
			'satuan' => 'pcs',
			'stok_warning' => 100,
			'id_jenis_barang' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('barang')->insert([
        	'kode_barang' => 'B002',
			'nama_barang' => 'AC Portable',
			'satuan' => 'pcs',
			'stok_warning' => 50,
			'id_jenis_barang' => 2,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
