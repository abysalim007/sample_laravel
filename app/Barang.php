<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    protected $table="barang";

    protected $fillable = [
    	'kode_barang',
    	'nama_barang',
		'satuan',
		'stok_warning',
		'id_jenis_barang',
    ]; 

    public function JenisBarang(){
    	return $this->hasOne('App\JenisBarang','id','id_jenis_barang');
    }
}
