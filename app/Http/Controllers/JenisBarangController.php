<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisBarang;
use DB;

class JenisBarangController extends Controller
{
    //
    public function index(){
    	$jb = JenisBarang::all();

    	return response()->json(['msg' => 'ok','data'=>$jb], 200);
    }

    public function find($id){
    	$jb = JenisBarang::where(['id'=>$id])->get();;
    	return $jb;
    }

    public function delete_data(Request $request){
    	$id_ = $request->post('id');
    	$jb = JenisBarang::find($id_);
    	$x = $jb->delete();

    	if ($x) {
    		return response()->json(['status' => 'sukses'], 200);
    	}else{
    		return response()->json(['status' => 'gagal'], 304);
    	}
    }

    public function save_data($id=0,Request $request){
    	
    	$dtObj = [
    		'jenis_barang' => $request->input('jenis_barang')
    	];

    	if ($id>0) {
    		$jb = JenisBarang::where(['id'=>$request->input('id')])->update($dtObj);
    	}else{
    		$dtObj['kode_barang'] = $request->input('kode_barang');
    		$jb = JenisBarang::create($dtObj);
    	}

    	if ($jb) {
    		return response()->json(['status' => 'sukses','data'=>$jb], 200);
    	}else{
    		return response()->json(['status' => 'gagal','data'=>[]], 304);
    	}
    }

    public function modal($type,$id){
    	if ($id>0) {
    		$data=$this->find($id)->toArray();
    		$dt['jenis_barang']=$data[0];
    	}
    	
    	switch ($type) {
    		case 'tambah':
    			return view('tambah_jb');
    			break;
    		case 'ubah':
    			return view('update_jb',$dt);
    			break;
    		default:
    			# view
    			return view('view_jb',$dt);
    			break;
    	}
    }

    public function dt_jenis_barang(Request $request){
    	$totalData = 0;
    	$records['draw'] = $request->input('draw');
	    $records['recordsTotal'] = $totalData;
	    $records['recordsFiltered'] = $totalData;
	    $records['data'] = [];
	    $start = $request->input('start');
	    $length = $request->input('length');
		$offset = empty($start) ? 0 : $start;
	    $rows = empty($length) ? 10 : $length;

	    // $conds = [];
	    $jenis_barang = $request->input('jenis_barang');
		// DB::enableQueryLog();
		$filter = [];
		if (!empty($jenis_barang)) {
			$filter['jenis_barang'] = $jenis_barang ;
		}
	    $objBarang = JenisBarang::query();
		foreach($filter as $field => $value){
		    $objBarang->where($field, 'LIKE', '%'.$value.'%');
		}
		$objBarang = $objBarang->distinct()->skip($offset)->take($rows)->get();
		// $q = DB::getQueryLog();
	    $totalData = $objBarang->skip($offset)->take($rows)->count();

	    if ($totalData>0) {
	    	foreach ($objBarang as $key => $row) {
	    		$records['data'][$key]['id'] = $row['id'];
	    		$records['data'][$key]['jenis_barang'] = $row['jenis_barang']; 
	    	}
	    }

	    return response()->json($records,($totalData>0)?200:404);
    }

    public function list(){
    	return view('list_jb');
    }
}
