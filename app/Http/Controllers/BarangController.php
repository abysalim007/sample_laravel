<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\JenisBarang;
use DB;

class BarangController extends Controller
{
    //
    public function index(){
    	$barang = Barang::with('jenisBarang')->get();

    	return response()->json(['msg' => 'ok','data'=>$barang], 200);
    }

    public function find($id){
    	$barang = Barang::where(['id'=>$id])->with('jenisBarang')->get();;
    	return $barang;
    }

    public function jenisBarang(){
    	$jb = JenisBarang::get();
    	return $jb;
    }

    public function list(){
    	return view('list_barang');
    }

    public function home(){
    	return view('home');
    }

    public function dt_barang(Request $request){

    	$totalData = 0;
    	// $records['draw'] = $_REQUEST['draw'];
    	$records['draw'] = $request->input('draw');
	    $records['recordsTotal'] = $totalData;
	    $records['recordsFiltered'] = $totalData;
	    $records['data'] = [];
	    $start = $request->input('start');
	    $length = $request->input('length');
		$offset = empty($start) ? 0 : $start;
	    $rows = empty($length) ? 10 : $length;

	    // $conds = [];
	    $nama_barang = $request->input('nama_barang');
	    $kode_barang = $request->input('kode_barang');
	    $stok_warning = $request->input('stok_warning');
	    $satuan = $request->input('satuan');
	    $jenis_barang = $request->input('jenis_barang');

	    $filter = [];
	    if (!empty($kode_barang)) {
	    	$filter['kode_barang'] = $kode_barang;
	    }
	    if (!empty($nama_barang)) {
	    	$filter['nama_barang'] = $nama_barang;
	    }
	    if (!empty($stok_warning)) {
	    	$filter['stok_warning'] = $stok_warning;
	    }
	    if (!empty($satuan)) {
	    	$filter['satuan'] = $satuan;
	    }
	    // if (!empty($jenis_barang)) {
	    // 	$filter['jenis_barang.jenis_barang'] = $jenis_barang;
	    // }
	    DB::enableQueryLog();
	    $objBarang = Barang::query()->with('jenisBarang');
	    foreach($filter as $field => $value){
	    	$search = strtolower($value);
		    $objBarang->where(trim($field), 'LIKE', "%{$search}%");
		}
	    
	    $objBarang = $objBarang->get()->skip($offset)->take($rows);
	    $q = DB::getQueryLog();

	    $totalData = $objBarang->count();
	    if ($totalData>0) {
	    	foreach ($objBarang as $key => $row) {
	    		$records['data'][$key]['id'] = $row['id'];
	    		$records['data'][$key]['nama_barang'] = $row['nama_barang']; 
	    		$records['data'][$key]['kode_barang'] = $row['kode_barang']; 
	    		$records['data'][$key]['satuan'] = $row['satuan'];
	    		$records['data'][$key]['stok_warning'] = $row['stok_warning'];
	    		$records['data'][$key]['jenis_barang'] = $row['jenisBarang']['jenis_barang'];
	    	}
	    }else{
	    	$records['draw'] = 0;
	    }

	    return response()->json($records,200);
    }

    public function modal($type,$id){
    	switch ($type) {
    		case 'tambah':
    			$jb = $this->jenisBarang()->toArray();
    			$dt['jb'] = $jb;
    			return view('tambah_barang',$dt);
    			break;
    		case 'ubah':
    			$data=$this->find($id)->toArray();
    			$dt['barang']=$data[0];
    			$jb = $this->jenisBarang()->toArray();
    			$dt['jb'] = $jb;
    			return view('update_barang',$dt);
    			break;
    		default:
    			# view
    			$data=$this->find($id)->toArray();
    			$data=$data[0];
    			return view('view_barang',$data);
    			break;
    	}
    }

    public function save_data($id=0,Request $request){
    	
    	$dtObj = [
    		'nama_barang' => $request->input('nama_barang'),
    		'satuan' => $request->input('satuan'),
    		'stok_warning' => $request->input('stok'),
    		'id_jenis_barang' => $request->input('jenis_barang'),
    	];

    	if ($id>0) {
    		$barang = Barang::where(['id'=>$request->input('id')])->update($dtObj);
    	}else{
    		$dtObj['kode_barang'] = $request->input('kode_barang');
    		$barang = Barang::create($dtObj);
    	}

    	if ($barang) {
    		return response()->json(['status' => 'sukses','data'=>$barang], 200);
    	}else{
    		return response()->json(['status' => 'gagal','data'=>[]], 304);
    	}
    }


    public function delete_data(Request $request){
    	$id_ = $request->post('id');
    	$barang = Barang::find($id_);
    	$x = $barang->delete();

    	if ($x) {
    		return response()->json(['status' => 'sukses'], 200);
    	}else{
    		return response()->json(['status' => 'gagal'], 304);
    	}
    }



}
