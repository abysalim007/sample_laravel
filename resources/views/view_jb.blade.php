<div class="modal fade" id="myView" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelTambah">Info Jenis Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Jenis Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" value="{{$jenis_barang['jenis_barang']}}" disabled="disabled" />
              </div>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myView').modal('show');
  });
</script>