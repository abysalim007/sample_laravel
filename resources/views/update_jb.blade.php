<div class="modal fade" id="myUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelTambah">Update Jenis Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Jenis Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="hidden" class="form-control" id="id_jb" value="{{$jenis_barang['id']}}"  />
                <input type="text" class="form-control" id="jb_update" value="{{$jenis_barang['jenis_barang']}}"  />
              </div>
            </div>
        </div>

       </div>
        
      <div class="modal-footer">
        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="save" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myUpdate').modal('show');

    $('#save').click(function(){
      simpanData();
    });
  });

  function simpanData(){
    var jenis_barang = $('#jb_update').val(); 
    var id_jb = $('#id_jb').val();
    var _token = '<?= csrf_token() ?>';

     if (jenis_barang.length==0) {

      alert('maaf data harus dilengkapi.');
     }else{
     //  /*save*/
        var url = "{{url('/simpan/jenis/barang')}}/"+id_jb; // url modul/function
        var data = {
          'jenis_barang':jenis_barang,
          'id':id_jb,
          '_token': _token,
        };

        $.post( url, data, function( resp ) {
          if(resp.status === 'sukses'){
            alert('data berhasil diupdate');
            window.location.reload();            
          }else{
            alert('gagal, terjadi kesalahan.');
          }

           $('#closeModal').trigger('click');

        }, "json");
     } 
  }
</script>