<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Daftar Jenis Barang</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="col-md-12 mt-5">
        	<h1>Daftar Jenis Barang</h1>
        	<div class="pull-right">
        		<a href="{{url('home')}}" class="btn btn-danger"><i class="fa fa-dashboard"></i> dashboard</a>
        		<a href="#" onclick="tambah(); return false;" class="btn btn-success"><i class="fa fa-plus"></i> tambah</a>
        		<a href="#" onclick="openFilter(); return false;" class="btn btn-info"><i class="fa fa-plus"></i> pencarian</a>
        	</div>
        	<table id="dt_jb" class="table table-striped table-bordered" style="width:100%">
		        <thead>
		            <tr>
		            	<th>#</th>
		                <th>Jenis Barang</th>
		                <th>Action</th>
		            </tr>
		        </thead>
		        <tbody></tbody>
		     </table>
        </div>
    </div>

    <div id="modal_filter">
    	<!-- filter form -->
    	<div class="modal fade" id="myFilter" tabindex="-1" role="dialog" aria-labelledby="myFilterLabel">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h4 class="modal-title" id="myFilterLabel">Filter</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        
			      </div>
			      <div class="modal-body" style="height: 180px">
			      	<div class="form-group">
			            <label class="col-md-2 control-label">Jenis Barang</label>
			            <div class="col-md-10">
			              <div class="input-group">
			                <span class="input-group-addon">
			                  <span class="fa fa-pencil-alt"></span>
			                </span>
			                <input type="text" class="form-control" id="cari_jenis_barang"/>
			              </div>
			            </div>
			        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Tutup</button>
			        <button type="button" id="cari" class="btn btn-primary">Cari</button>
			      </div>
			  </div>
			</div>
	    </div>
	</div>
 
    <div id="modal_form">
    	<!-- all form will show here -->
    </div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
	    get_data();
	  });

	$('#cari').click(function(){
	  /*custom form filter*/
	  filter_table();
	});

	/*---------------------------------------------------------------------*/
  	function openFilter(){
  		$('#myFilter').modal('show');
  	}

  	function filter_table(){
  		if ($.fn.dataTable.isDataTable("#dt_jb")) {
	    	$('#dt_jb').DataTable().destroy();  
	  	}
	  	var _token = '<?= csrf_token() ?>';
	  	var jenis_barang = $('#cari_jenis_barang').val();
	  	var t = $('#dt_jb').DataTable( {
	        "serverSide": true,
	        "searching" : false,
	        "paging":   true,
	        "ordering": false,
	        "info":     false,
	        "ajax": {
	            "url": '<?= url("/dt/jenis/barang") ?>',
	            "dataSrc": 'data',
	            "type" : "POST",
	            "data": {
	            	"_token": _token,
	            	"jenis_barang" : jenis_barang
	            },
	        },
	        "order": [[ 1, 'asc' ]],
	        "iDisplayLength": 10,
	        "columns": [
	            { "data": 'id', "orderable": false, 
	                render: function (data, type, row, meta) {
	                return meta.row + meta.settings._iDisplayStart + 1;
	                }
	            },
	            { "data": 'jenis_barang', "orderable": false },
	            { "data": 'act', "orderable": false,
	            	render: function (data, type, row, meta) {
	                	var htmlBtn = '';
	                	htmlBtn +='<a href="#" onclick="view('+row.id+');return false;" class="btn btn-sm btn-info">view</a>';
	                	htmlBtn +='<a href="#" onclick="ubah('+row.id+');return false;" class="btn btn-sm btn-success">ubah</a>';
	                	htmlBtn +='<a href="#" onclick="hapus('+row.id+');return false;" class="btn btn-sm btn-danger">hapus</a>';
	                	return htmlBtn;
	                }
	            },
	        ]
	    });
  	}

  	function get_data(){
  		if ($.fn.dataTable.isDataTable("#dt_jb")) {
	    	$('#dt_jb').DataTable().destroy();  
	  	}
	  	var _token = '<?= csrf_token() ?>';
	  	var t = $('#dt_jb').DataTable( {
	        "serverSide": true,
	        "searching" : false,
	        "paging":   true,
	        "ordering": false,
	        "info":     false,
	        "ajax": {
	            "url": '<?= url("/dt/jenis/barang") ?>',
	            "dataSrc": 'data',
	            "type" : "POST",
	            "data": {"_token": _token},
	        },
	        "order": [[ 1, 'asc' ]],
	        "iDisplayLength": 10,
	        "columns": [
	            { "data": 'id', "orderable": false, 
	                render: function (data, type, row, meta) {
	                return meta.row + meta.settings._iDisplayStart + 1;
	                }
	            },
	            { "data": 'jenis_barang', "orderable": false },
	            { "data": 'act', "orderable": false,
	            	render: function (data, type, row, meta) {
	                	var htmlBtn = '';
	                	htmlBtn +='<a href="#" onclick="view('+row.id+');return false;" class="btn btn-sm btn-info">view</a>';
	                	htmlBtn +='<a href="#" onclick="ubah('+row.id+');return false;" class="btn btn-sm btn-success">ubah</a>';
	                	htmlBtn +='<a href="#" onclick="hapus('+row.id+');return false;" class="btn btn-sm btn-danger">hapus</a>';
	                	return htmlBtn;
	                }
	            },
	        ]
	    });
  	}
	

	function tambah(){
	  url = '<?= url("/jenis/barang") ?>';
	  id = 0;
	  type = 'tambah'
	  getUrl = url +'/'+ type +'/'+ id+'/form';
	  $.ajax({
	        async:true, 
	        type:'get',
	        complete:function(request, json) {
	            $('#modal_form').html(request.responseText);
	        }, 
	        url:getUrl
	    })
	}

	function ubah(id){
	  url = '<?= url("/jenis/barang") ?>';
	  id = id;
	  type = 'ubah'
	  getUrl = url +'/'+ type +'/'+ id+'/form';
	  $.ajax({
	        async:true, 
	        type:'get',
	        complete:function(request, json) {
	            $('#modal_form').html(request.responseText);
	        }, 
	        url:getUrl
	    })
	}

	function view(id){
	  url = '<?= url("/jenis/barang") ?>';
	  id = id;
	  type = 'view'
	  getUrl = url +'/'+ type +'/'+ id+'/form';
	  $.ajax({
	        async:true, 
	        type:'get',
	        complete:function(request, json) {
	            $('#modal_form').html(request.responseText);
	        }, 
	        url:getUrl
	    })
	}

	function hapus(id){
	  var  confrm =  confirm("Yakin menghapus data!");
	  var _token = '<?= csrf_token() ?>';
	  if (confrm == true) {
	  	var url = '<?= url("/hapus/jenis/barang") ?>';
	  	$.post( url, {
	  		'id' : id,
	  		'_token': _token,
	  	}, function( resp ) {
	  		if (resp.status=='sukses') {
	  			location.reload();
	  		}else{
	  			alert('data gagal dihapus');
	  		}
	  	});
	  }
	}

	
</script>
</html>
