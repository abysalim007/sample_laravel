<div class="modal fade" id="myModalTambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelTambah">Tambah Jenis Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Jenis Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="jenis_barang_in"/>
              </div>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="save" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#save').click(function(){
      simpanData();
    });

    $('#myModalTambah').modal('show');


    function simpanData(){
      var jenis_barang = $('#jenis_barang_in').val(); 
      var _token = '<?= csrf_token() ?>';

       if (jenis_barang.length==0) {
        alert('maaf data harus dilengkapi.');
       }else{
        /*save*/
          var url = "{{url('/simpan/jenis/barang/0')}}"; // url modul/function
          var data = {
            'jenis_barang':jenis_barang,
            '_token': _token,
          };

          $.post( url, data, function( resp ) {
            if(resp.status === 'sukses'){
              alert('data berhasil disimpan');
              window.location.reload();            
            }else{
              alert('gagal, terjadi kesalahan.');
            }

             $('#closeModal').trigger('click');

          }, "json");
       } 
    }
  });
</script>