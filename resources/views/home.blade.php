<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">Main Menu</h3>
                </div>
                <div class="card-body">
                    <div class="col-md-12 row">
                        <div class="col-md-6">
                            <div class="alert alert-success">
                                <a href="{{url('/list/barang')}}" class="btn btn-default">List Data Barang</a>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-warning">
                                <a href="{{url('/list/jenis/barang')}}" class="btn btn-default">List Jenis Barang</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="card-footer text-center">
                    <a href="{{route('logout') }}" class="btn btn-danger">Logout</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>