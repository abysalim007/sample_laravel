<div class="modal fade" id="myModalTambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelTambah">Tambah Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Kode Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="kode_barang_in" placeholder="contoh : B001" />
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Nama Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="nama_barang_in"/>
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Satuan</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="satuan_in"/>
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Stok Warning</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="number" class="form-control" id="stok_in"/>
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Jenis Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <select class="form-control" id="jenis_barang_in">
                  <option value="">--Pilih--</option>
                  @foreach ($jb as $jb)
                  <option value="{{$jb['id']}}">{{$jb['jenis_barang']}}</option>
                  @endforeach
                </select>
              </div>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="save" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#save').click(function(){
      simpanData();
    });

    $('#myModalTambah').modal('show');


    function simpanData(){
      var kode_barang = $('#kode_barang_in').val(); 
      var nama_barang = $('#nama_barang_in').val(); 
      var satuan = $('#satuan_in').val(); 
      var stok = $('#stok_in').val(); 
      var jenis_barang = $('#jenis_barang_in').val();
      var _token = '<?= csrf_token() ?>';

       if ((nama_barang.length==0) || (satuan.length==0) || (stok.length==0) || (jenis_barang.length==0)) {
        alert('maaf data harus dilengkapi.');
       }else{
       //  /*save*/
          var url = "{{url('/simpan/barang/0')}}"; // url modul/function
          var data = {
            'kode_barang':kode_barang,
            'nama_barang':nama_barang,
            'satuan':satuan,
            'stok':stok,
            'jenis_barang':jenis_barang,
            '_token': _token,
          };

          $.post( url, data, function( resp ) {
            if(resp.status === 'sukses'){
              alert('data berhasil disimpan');
              window.location.reload();            
            }else{
              alert('gagal, terjadi kesalahan.');
            }

             $('#closeModal').trigger('click');

          }, "json");
       } 
    }
  });
</script>