<div class="modal fade" id="myView" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelTambah">Info Barang</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Kode Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="kode_barang" value="{{$kode_barang}}" disabled="disabled" />
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Nama Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="nama_barang" value="{{$nama_barang}}" disabled="disabled" />
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Satuan</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="satuan" value="{{$satuan}}" disabled="disabled" />
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Stok Warning</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="number" class="form-control" id="stok" value="{{$stok_warning}}" disabled="disabled"/>
              </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Jenis Barang</label>
            <div class="col-md-10">
              <div class="input-group">
                <span class="input-group-addon">
                  <span class="fa fa-pencil-alt"></span>
                </span>
                <input type="text" class="form-control" id="jenis_barang" value="{{$jenis_barang['jenis_barang']}}" disabled="disabled" />
              </div>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="closeModal" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#myView').modal('show');
    // alert('tampilkan modal');
  });
</script>