<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');
Route::get('register', 'AuthController@showFormRegister')->name('register');
Route::post('register', 'AuthController@register');

// Route::post('api/auth', 'AuthController@api_login');
 
Route::group(['middleware' => 'auth'], function () {
	
	Route::get('home', 'BarangController@home')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');
 	
 	/*api content*/
 	/*list*/
 	Route::get('list/barang', 'BarangController@list')->name('view_barang');
 	Route::get('list/jenis/barang', 'JenisBarangController@list')->name('view_jenis_barang');

 	/*dt format*/
 	Route::post('dt/barang', 'BarangController@dt_barang')->name('dt_barang');
 	Route::post('dt/jenis/barang', 'JenisBarangController@dt_jenis_barang')->name('dt_jenis_barang');

 	/*modal*/
 	Route::get('/panel/barang/{type}/{id}/form', array(
	    'as' => 'modal_form',
	    'uses' => 'BarangController@modal'
	));
	Route::get('/jenis/barang/{type}/{id}/form', array(
	    'as' => 'modal_form',
	    'uses' => 'JenisBarangController@modal'
	));

 	/*create & update*/
 	Route::post('/simpan/barang/{id}', array(
	    'as' => 'simpan_data',
	    'uses' => 'BarangController@save_data'
	));
	Route::post('/simpan/jenis/barang/{id}', array(
	    'as' => 'simpan_data',
	    'uses' => 'JenisBarangController@save_data'
	));
 	
 	/*read*/
 	Route::get('get/barang/{id}', 'BarangController@find')->name('find_barang');
	Route::get('get/jenis/barang/{id}', 'JenisBarangController@find')->name('find_jenis_barang'); 
 	
 	/*delete*/
 	Route::post('/hapus/barang', array(
	    'as' => 'hapus_data',
	    'uses' => 'BarangController@delete_data'
	));
	Route::post('/hapus/jenis/barang', array(
	    'as' => 'hapus_data',
	    'uses' => 'JenisBarangController@delete_data'
	));
});


